# Knowledge, Skills and Experience


## 1. knowledge domain structure
![Image title](../assets/knowledge_domain_structure.png){ align=center }


## 2. knowledge domain mining


## 3. Information layers

![Image title](../assets/inter_memory_layers.png){ align=center }
### 3.1. Knowledge layer
### 3.2. Skills layer
### 3.3. Experience layer

## 4. Experiences monetization