# Introduction
In a world plagued with multifaceted challenges, artificial intelligence (AI) emerges as a potent toolbox fostering innovation and delivering robust solutions. AI, when wielded adeptly, can align product reliability, environmental sustenance, and business ambitions such as time-to-market efficiency and competitive edge, providing resolutions to health, environmental, and societal issues.

# Definitions
## Design Thinking 
The design thinking process usualy used to develop new projects that the owner has not a clear vision about the interest of creating new one, this could be an internal or external stakeholder.
The process is defined in a number of steps that contains the following:

Problem Space

* Understand
* Observe
* Define a point of view

Solution Space

* Ideate
* Prototype 
* Test

Link between the Problem and Solution space

* Reflect

## Behavioral Science 
Behavioral science is a field of study that looks at how people act and what makes them act that way. It uses things like psychology, sociology, economics, and neuroscience to figure out why people do what they do, how they interact with each other and their surroundings, and how behavior can be changed or influenced. Behavioral scientists use research and data analysis to find patterns and insights about how people act. These can be used in many fields, such as business, healthcare, public policy, and more. It is a way to understand and predict how people will act and react by using information from many different fields.

## Systems Engineering
Systems engineering is an interdisciplinary branch of engineering that focuses on designing, putting together, and running systems that are complicated. It is an approach to engineering that looks at the whole system and how its parts work together, not just the parts on their own. Here are some important things to know about systems engineering:

* Holistic Approach: Systems engineers look at a project or problem as a whole and think about how all of its parts work together. This includes hardware, software, people, processes, the environment, and the way things work.

* Requirements Analysis: One of the most important steps is to define and understand the needs and requirements of the system's stakeholders. This means getting information from many different places to make sure the system will meet its goals.

* Design and Integration: Systems engineers work on the design of the whole system, making sure that all of its parts fit together and work well as a whole. They think about trade-offs and optimizations to get the performance they want.

* Verification and Validation: The system is tested and validated by systems engineers to make sure it meets the requirements. This includes testing both the parts on their own and the whole system.

* Lifecycle Management: Systems engineering looks at all parts of a system's life, from its idea and design to its creation, use, and eventual retirement or replacement.

* Risk Management: One of the most important parts of systems engineering is finding and reducing risks. This means fixing any problems that might come up during development, deployment, or operation.

* Interdisciplinary Collaboration: Systems engineers often work with experts from different fields, like electrical engineering, mechanical engineering, software engineering, and more, to come up with complete solutions.

* Documentation: Systems engineering requires detailed documentation to make sure that all parts of the system are understood, especially by those who maintain and run it.

## Goals
The advocated approach here is intrinsically human-centric, endeavoring to alleviate cognitive strain, and ensuring the human intellect is predominantly engaged in high-caliber functionalities such as creativity and innovation.

AI’s role is pivotal in refining and substituting aspects of systems engineering activities and processes, accelerating the delivery of software and hardware with impeccable quality, and redirecting engineers towards more productive and imaginative pursuits.

![Image title](assets/combined_concepts_processes.png){ align=center }

# Systems engineering process
## Traditional way
``` mermaid
graph LR
    subgraph Conceptualization
        Vis[Visualisations] -.Captured.-> RE[Requirements engineering];
        Exp[Expectations]-.Captured.-> RE;
        Gls[Goals]-.Captured.-> RE;
        RE <--> SD[Software design];
        RE  <--> HD[Hardware design];
    end

    subgraph Creation
        SD  <--> SC[Software construction];
        HD  <--> HC[Hardware construction];
    end

    subgraph Realization
        SC  --> Int[Integration];
        HC  --> Int[Integration];
        Int --> Prd[Product];
        Prd --> Dpl[Deployement];
    end
    subgraph "Verification & Validation"
        Sim[Simulation];
        Emul[Emulation];
        Test[Testing];
    end

```
## Suggested systems engineering way
``` mermaid
graph TB
    subgraph "Conceptual Innovations"
        DT[Design Thinking] --> AI[AI Algorithms]
        BS[Behavioral Science] --> AI
    end
    AI --> SE[Systems Engineering]
    SE --> P[Product]
```

# Roadmap
The incorporation of AI into the systems engineering process stands to significantly enhance efficiency, productivity, and innovation, allowing humans to delve deeper into creative endeavors, thereby ushering in advanced and sustainable solutions to prevailing global quandaries.

* First Stage: Design Thinking and Requirements Engineering
    * Documents Digestion: Use AI for analyzing and understanding documents.
    * Documents Indexation: Implement AI for indexing documents for efficient retrieval.
    * Requirements Extraction: Employ AI to extract and define requirements from the digested documents.
    * Requirements Approval: Establish an iterative approach using AI for approving requirements.
    * Requirements Classification: Utilize AI for categorizing the extracted requirements.
    * Requirements Prioritization: Leverage AI to prioritize requirements based on business needs.
    * Requirements Allocation and Mapping: Apply AI to allocate and map requirements to suitable system constructs.

# Design Thinking & Systems engineering processes merging

![Image title](assets/SE-I_O.png){ align=left }

Using borrowed principles from design thinking to define a problem statement that the solution is capable of answering, thus merging the two processes into one process that starts with understanding and concludes with iterative micro-releases of the software and the hardware artifacts will aligning with the stakeholders vision and expectations using behavioral science tactics and strategies.


