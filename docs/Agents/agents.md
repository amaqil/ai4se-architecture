# Requirements Engineering using AI Agents

### Overview

Multi-Agent Systems (MAS) provide a collaborative approach to resolving intricate issues by dividing responsibilities among several agents. In the context of System Engineering, particularly in the Requirements Process, agents can be utilized to automate, improve, and ensure efficiency throughout each phase. This document presents a customized set of agents for this purpose.

## Unit Agents

### 1. Stakeholder Interaction Agent (RE)
- **Input:** Stakeholder communications, initial feedback.
- **Output:** Categorized stakeholder list, captured needs, desires, and constraints.
- **Functions:** 
    - Facilitate initial stakeholder communication.
    - Categorize stakeholders (Optimal, Normal, Widespread, Not Engaged).
    - Capture stakeholder needs, desires, and constraints.

### 2. Data Collection Agent (RE)
- **Input:** Channels to conduct surveys, interviews, and observations.
- **Output:** Collated and processed data.
- **Functions:** 
    - Administer surveys.
    - Conduct interviews.
    - Log observational data.

### 3. Use Case Generator (RE)
- **Input:** Gathered requirements.
- **Output:** Drafted use cases.
- **Functions:** Automatically draft potential use cases based on requirements.

### 4. Conflict Identifier (RA)
- **Input:** Set of gathered requirements.
- **Output:** List of ambiguous or contradictory requirements.
- **Functions:** Identify conflicting or ambiguous requirements.

### 5. Requirement Prioritizer (RA)
- **Input:** Requirements, stakeholder feedback, business goals, technical constraints.
- **Output:** Prioritized list of requirements.
- **Functions:** Use algorithms (like LLMS, Genetic) to determine requirement importance.

### 6. Traceability Tracker (RA)
- **Input:** Requirements, their sources.
- **Output:** Traced requirements.
- **Functions:** Ensure each requirement is linked to its source.

### 7. Functional Decomposer (RP)
- **Input:** Complex requirements.
- **Output:** Granular tasks.
- **Functions:** Break complex requirements into actionable tasks.

### 8. Visualization Agent (RP)
- **Input:** Requirements.
- **Output:** Diagrams, flowcharts, and visual models.
- **Functions:** Represent requirements visually.

### 9. Verification Checker (RVV)
- **Input:** Documented requirements.
- **Output:** Assessment results.
- **Functions:** Check clarity and quality of requirements.

### 10. Validation Agent (RVV)
- **Input:** Documented requirements, stakeholder groups.
- **Output:** Validation results.
- **Functions:** Seek feedback to ensure requirements align with stakeholder intentions.



```puml
@startuml
class "Stakeholder Interaction Agent (RE)" {
    - Input: Stakeholder communications, initial feedback
    - Output: Categorized stakeholder list, captured needs, desires, and constraints
    --
    + Facilitate initial stakeholder communication()
    + Categorize stakeholders()
    + Capture stakeholder needs, desires, and constraints()
}

class "Data Collection Agent (RE)" {
    - Input: Channels to conduct surveys, interviews, and observations
    - Output: Collated and processed data
    --
    + Administer surveys()
    + Conduct interviews()
    + Log observational data()
}

class "Use Case Generator (RE)" {
    - Input: Gathered requirements
    - Output: Drafted use cases
    --
    + Draft potential use cases based on requirements()
}

class "Conflict Identifier (RA)" {
    - Input: Set of gathered requirements
    - Output: List of ambiguous or contradictory requirements
    --
    + Identify conflicting or ambiguous requirements()
}

class "Requirement Prioritizer (RA)" {
    - Input: Requirements, stakeholder feedback, business goals, technical constraints
    - Output: Prioritized list of requirements
    --
    + Determine requirement importance using algorithms()
}

class "Traceability Tracker (RA)" {
    - Input: Requirements, their sources
    - Output: Traced requirements
    --
    + Link each requirement to its source()
}

class "Functional Decomposer (RP)" {
    - Input: Complex requirements
    - Output: Granular tasks
    --
    + Break complex requirements into actionable tasks()
}

class "Visualization Agent (RP)" {
    - Input: Requirements
    - Output: Diagrams, flowcharts, and visual models
    --
    + Represent requirements visually()
}

class "Verification Checker (RVV)" {
    - Input: Documented requirements
    - Output: Assessment results
    --
    + Check clarity and quality of requirements()
}

class "Validation Agent (RVV)" {
    - Input: Documented requirements, stakeholder groups
    - Output: Validation results
    --
    + Seek feedback to ensure alignment with stakeholder intentions()
}

@enduml
```

## Super Agents

### 1. Requirements Discovery Master (RE)
- **Comprises:** Stakeholder Interaction Agent, Data Collection Agent, Use Case Generator.
- **Functions:** Oversee and optimize the requirements gathering phase.

### 2. Analysis and Reconciliation Expert (RA)
- **Comprises:** Conflict Identifier, Requirement Prioritizer, Traceability Tracker.
- **Functions:** Ensure clear, prioritized, and traceable requirements.

### 3. Presentation Maestro (RP)
- **Comprises:** Functional Decomposer, Visualization Agent.
- **Functions:** Create clear and visual representations of requirements.

### 4. Verification and Validation Guru (RVV)
- **Comprises:** Verification Checker, Validation Agent.
- **Functions:** Ensure correct documentation and genuine representation of stakeholder needs.

### 5. End-to-End Requirements Orchestrator
- **Input:** Outputs from all super agents.
- **Functions:** Oversee the entire requirements process, ensuring smooth transition and integration between each stage.

```puml
@startuml

class "Requirements Discovery Master (RE)" {
    - Comprises: Stakeholder Interaction Agent, Data Collection Agent, Use Case Generator
    --
    + Oversee and optimize the requirements gathering phase()
}

class "Analysis and Reconciliation Expert (RA)" {
    - Comprises: Conflict Identifier, Requirement Prioritizer, Traceability Tracker
    --
    + Ensure clear, prioritized, and traceable requirements()
}

class "Presentation Maestro (RP)" {
    - Comprises: Functional Decomposer, Visualization Agent
    --
    + Create clear and visual representations of requirements()
}

class "Verification and Validation Guru (RVV)" {
    - Comprises: Verification Checker, Validation Agent
    --
    + Ensure correct documentation and genuine representation of stakeholder needs()
}

class "End-to-End Requirements Orchestrator" {
    - Input: Outputs from all super agents
    --
    + Oversee the entire requirements process()
}

@enduml

```

## Stakeholder Involvement

Stakeholders play a crucial role in the requirements engineering process. Their feedback, desires, constraints, and needs are essential to ensure the system is designed to meet its intended purposes. They are involved in:

1. **Initial Communication:** To voice their needs, desires, and constraints.
2. **Validation:** To ensure the documented requirements genuinely represent their needs.

```puml
@startuml

participant "Stakeholder" as S
participant "Stakeholder Interaction Agent (RE)" as SIA
participant "Data Collection Agent (RE)" as DCA
participant "Use Case Generator (RE)" as UCG
participant "Requirements Discovery Master (RE)" as RDM

S -> SIA : Initial Communication (Needs, Desires, Constraints)
SIA -> SIA : Categorize Stakeholder
SIA -> RDM : Send Categorized Stakeholder Data

S -> DCA : Participate in Surveys/Interviews
DCA -> DCA : Collate Data
DCA -> RDM : Send Processed Data

RDM -> UCG : Send Gathered Requirements
UCG -> UCG : Generate Use Cases
UCG -> RDM : Send Drafted Use Cases

RDM -> S : Validate Drafted Use Cases

@enduml
```

This diagram showcases the interactions between the stakeholder and various agents involved in the requirements discovery phase. It highlights the initial communication from stakeholders, the processing of their feedback, the generation of use cases, and the validation of those use cases.

## Agents Interaction

Agents operate in a tiered hierarchy:

```puml
@startuml
package "End-to-End Requirements Orchestrator" { 
 package "Verification and Validation Master Agent (RVV)" {
    [Verification Checker (RVV)]
    [Validation Agent (RVV)]
  }
  
  package "Presentation Master Agent(RP)" {
    [Functional Decomposer (RP)]
    [Visualization Agent (RP)]
  }  
package "Analysis and Reconciliation Master Agent (RA)" {
    [Conflict Identifier (RA)]
    [Requirement Prioritizer (RA)]
    [Traceability Tracker (RA)]
  }

package "Requirements Discovery Master Agent (RE)" {
    [Stakeholder Interaction Agent (RE)]
    [Data Collection Agent (RE)]
    [Use Case Generator (RE)]
  }

}
@enduml
```

1. **Unit Agents:** These agents carry out specific tasks related to different phases of requirements engineering.
2. **Super Agents:** These integrate the functionalities of multiple unit agents, offering a more comprehensive and cohesive approach to major phases in the lifecycle.
3. **End-to-End Orchestrator:** This agent integrates the outputs of super agents, ensuring a seamless transition between different stages.

The interaction between agents is bidirectional. While super agents can instruct unit agents, unit agents also feedback results to super agents for a more consolidated output.

```puml
@startuml

participant "Requirements Discovery Master (RE)" as RDM
participant "Analysis and Reconciliation Expert (RA)" as ARE
participant "Presentation Maestro (RP)" as PM
participant "Verification and Validation Guru (RVV)" as VVG
participant "End-to-End Requirements Orchestrator" as E2E

RDM -> ARE : Send Drafted Use Cases
ARE -> ARE : Identify Conflicts
ARE -> ARE : Prioritize Requirements
ARE -> ARE : Trace Requirements to Sources
ARE -> E2E : Send Analyzed Requirements

E2E -> PM : Send Analyzed Requirements
PM -> PM : Decompose Functions
PM -> PM : Visualize Requirements
PM -> E2E : Send Visual Representations

E2E -> VVG : Send Visual Representations
VVG -> VVG : Verify Clarity and Quality
VVG -> VVG : Validate with Stakeholders
VVG -> E2E : Send Verified and Validated Requirements

@enduml

```

</details>
This diagram depicts the lifecycle of a requirement from its origin as a use case, moving through analysis, presentation, and culminating in verification and validation. The flow between the super agents illustrates the streamlined process of refining and finalizing system requirements.
